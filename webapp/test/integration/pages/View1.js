sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/actions/Press",
	"sap/ui/test/matchers/PropertyStrictEquals",
	"sap/ui/test/matchers/BindingPath"
], function (Opa5, Press, PropertyStrictEquals, BindingPath) {
	"use strict";
	var sViewName = "View1";
	Opa5.createPageObjects({
		onTheAppPage: {

			actions: {
				iPressOnTheButton: function (sId) {
					return this.waitFor({
						controlType: "sap.m.Button",
						viewName: sViewName,
						id: "viewButtonOne",
						//	actions: new Press(),
						success: function (aBtn) {
							aBtn.$().trigger("tap");
							Opa5.assert.ok(true, "Button is clicked");
						},
						errorMessage: "Not able to find the button"
					});
				},
				iPressOnTheListItem: function (sId) {
					return this.waitFor({
						controlType: "sap.m.Table",
						viewName: sViewName,
						id: "idEmployeeList",
						actions: new Press(),
						success: function (aBtn) {
							var src = aBtn.getAggregation("items")[0];
							src.$().trigger("tap");
							Opa5.assert.ok(true, "List item is pressed....");
						},
						errorMessage: "Not able to find the list control"
					});
				}
			},

			assertions: {

				iShouldSeeTheApp: function () {
					return this.waitFor({
						id: "app",
						viewName: sViewName,
						success: function () {
							Opa5.assert.ok(true, "The View1 view is displayed");
						},
						errorMessage: "Did not find the View1 view"
					});
				},
				checkIfButtonLabelChangd: function () {
					return this.waitFor({
						id: "viewButtonOne",
						viewName: sViewName,
						matchers: new PropertyStrictEquals({
							name: "text",
							value: "i was clicked"
						}),
						success: function () {
							Opa5.assert.ok(true, "The Button text is changes successfully");
						},
						errorMessage: "Something went wrong"
					});
				},
				islisteItemPressed: function () {
					return this.waitFor({
						id: "idEmployeeList",
						viewName: sViewName,

						success: function (oEvnt) {
							Opa5.assert.ok(true, "The List Item is pressed successfully");
						},
						errorMessage: "Something went wrong on list items"
					});
				}
			}
		}
	});

});