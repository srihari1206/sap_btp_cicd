	/*global QUnit*/

	sap.ui.define([
		"sap/ui/test/opaQunit",
		"./pages/View1"
	], function (opaTest) {
		"use strict";

		QUnit.module("Navigation Journey");

		opaTest("Should see the initial page of the app", function (Given, When, Then) {
			// Arrangements
			Given.iStartMyApp();
			//actions
			When.onTheAppPage.iPressOnTheButton();
			// Assertions
			Then.onTheAppPage.iShouldSeeTheApp();
			Then.onTheAppPage.checkIfButtonLabelChangd();

		});
		opaTest("able to click on the liste items", function (Given, When, Then) {

			//actions
			When.onTheAppPage.iPressOnTheListItem();
			// Assertions
			Then.onTheAppPage.islisteItemPressed();

			//Cleanup
			Then.iTeardownMyApp();
		});
	});