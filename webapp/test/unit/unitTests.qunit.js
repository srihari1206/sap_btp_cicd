/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/yash/opa5/OPA/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});