sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (Controller, MessageBox) {
	"use strict";

	return Controller.extend("com.yash.opa5.OPA.controller.View1", {
		onInit: function () {

		},
		onPress: function (oEvent) {
			var obt = oEvent.getSource();
			obt.setText("i was clicked");
			console.log("console printing");
			var oModel = new sap.ui.model.json.JSONModel();
			var that = this;
			var aData = jQuery.ajax({
				type: "GET",
				contentType: "application/json",
				url: "/NorthwindService/V2/Northwind/Northwind.svc/Employees",
				dataType: "json",
				async: false,
				success: function (data, textStatus, jqXHR) {
					this.getView().getModel("JsonModel").setProperty("/Employees", data.d.results);
					//	oModel.setData(data);
				}.bind(this)
			});
			//	this.getView().setModel(oModel);

			/*this.getView().getModel().read("/Employees", {
				success: function (oData) {
					this.getView().getModel("JsonModel").setProperty("/Employees", oData.results);
				}.bind(this),
				error: function (oError) {
					//	console.log("odata error");
				}
			});*/
		},
		onEmployeePress: function (oEvent) {
			var sPath = oEvent.getSource().getBindingContext("JsonModel").getPath();
			MessageBox.show("List item is clicked");
		}
	});
});